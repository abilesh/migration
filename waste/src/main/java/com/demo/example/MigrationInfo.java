package com.demo.example;

import java.util.HashMap;
import java.util.Map;

import jdk.nashorn.internal.parser.TokenType;

//srcTblName ; targetTbleName ; srcColName ; targetColName ;
public class MigrationInfo
{
	public static String mySelectString;
	public static  String myInsertString;
	private String res1;
	private String res2;
	@Override
	public String toString()
	{
		return "MigrationInfo[" + "mySelectString='" +
				mySelectString + '\'' +
				", myInsertString='" +
				myInsertString + '\'' +
				']';
	}
	public MigrationInfo MigrateTable(IATableInfo iaTableInfo)
	{
		MigrationInfo migrationInfo = new MigrationInfo();
		IAMigrate iaMigrate = new IAMigrate();
	      System.out.println("no of tables: "+iaMigrate.value1);
		for(int tables = 0; tables < iaMigrate.value1; tables++)
		{
			
			migrationInfo.mySelectString = "SELECT ";
			for (int temp = 0; temp < iaTableInfo.iaColumns.size(); temp++) {
				migrationInfo.mySelectString += iaTableInfo.iaColumns.get(temp).getSrcColName();
				migrationInfo.mySelectString += ",";
			}
			migrationInfo.mySelectString = removeLastCharacter(migrationInfo.mySelectString);
			migrationInfo.mySelectString += " FROM " + iaTableInfo.getSrcTblName();

			migrationInfo.myInsertString = "INSERT INTO "+iaTableInfo.getTargetTbleName()+" (";
			for(int temp = 0; temp<iaTableInfo.iaColumns.size(); temp++)
			{
				migrationInfo.myInsertString += iaTableInfo.iaColumns.get(temp).getTargetColName();
				migrationInfo.myInsertString += ",";
			}
			migrationInfo.myInsertString = removeLastCharacter(migrationInfo.myInsertString);
			migrationInfo.myInsertString += ") VALUES (";
			for(int temp = 0; temp<iaTableInfo.iaColumns.size(); temp++)
			{
				migrationInfo.myInsertString += "?,";
			}
			migrationInfo.myInsertString = removeLastCharacter(migrationInfo.myInsertString);
			migrationInfo.myInsertString += ")";
		}

		//System.out.println(migrationInfo.mySelectString);
		//System.out.println(migrationInfo.myInsertString);
		System.out.println(migrationInfo);
		return migrationInfo;
	}

	public String removeLastCharacter(String str)
	{
		System.out.println("remove_last_char"+str);
		String result = null;
		if ((str != null) && (str.length() > 0))
		{
			result = str.substring(0, str.length() - 1);
		}
		System.out.println("returning..."+result);
		return result;
	}
}