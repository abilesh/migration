package com.infotech.app;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.xml.sax.SAXException;

import com.demo.example.IAColumnInfo;
import com.demo.example.IAMigrate;
import com.demo.example.IATableInfo;
import com.demo.example.MigrationInfo;
import com.infotech.app.dao.UserDAO;
import com.infotech.app.model.User;

@SpringBootApplication
public class SpringBootJdbcApplication implements CommandLineRunner{

	@Autowired
	private UserDAO userDAO;
	
	public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException {
		SpringApplication.run(SpringBootJdbcApplication.class, args);
		
		

	}

	@Override
	public void run(String... args) throws Exception {
		
		
		IAColumnInfo ia = new IAColumnInfo();
		IAMigrate im = new IAMigrate();
		List<IATableInfo> myTables = im.migratexml();
		System.out.println("hiileo"+myTables);
		MigrationInfo migrationInfo = new MigrationInfo();
		
		for(int temp =0; temp <im.value1;temp++ )
			migrationInfo.MigrateTable(myTables.get(temp));
		
		System.out.println("getselect:**"+MigrationInfo.mySelectString);
		System.out.println("getinsert:**"+MigrationInfo.myInsertString);
		String finalquery1=MigrationInfo.mySelectString;
		System.out.println("now_wat-i_want"+finalquery1);
		String finalquery2=MigrationInfo.myInsertString;
		System.out.println("now_wat-i_want"+finalquery2);
		List<IAColumnInfo> list = new ArrayList<IAColumnInfo>();
		list=getUser(finalquery2);
		cerateUser(list,finalquery2);

	}

	private List<IAColumnInfo> getUser(String finalquery2) {
		List<IAColumnInfo> li  = userDAO.getUser(finalquery2);
		//System.out.println("elements"+li);
		return li;
	}

	private void cerateUser(List<IAColumnInfo> li,String finalquery1) {
			userDAO.createUser(li,finalquery1);
	}
}
