package com.infotech.app.dao;

import java.util.List;

import com.demo.example.IAColumnInfo;
import com.infotech.app.model.User;

public interface UserDAO {
	public abstract void createUser(List<IAColumnInfo> li, String finalquery1);
	public abstract List<IAColumnInfo> getUser(String finalquery2);

}
